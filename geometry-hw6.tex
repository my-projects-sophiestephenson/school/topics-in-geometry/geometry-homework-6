\documentclass[12pt, twoside]{amsart}

\newcommand{\R}{{\mathbb R}}
\newcommand{\Z}{{\mathbb Z}}
\newcommand{\C}{{\mathbb C}}
\newcommand{\xn}{{\vec x_n}}
\newcommand{\bigH}{{\mathbb H}}
\newcommand{\Rbar}{\overline{\mathbb R}}

\setlength{\textwidth}{6.5in}
\setlength{\textheight}{8.5in}
\setlength{\oddsidemargin}{0in}
\setlength{\evensidemargin}{0in}
\setlength{\topmargin}{0in}

\everymath{\displaystyle}
\usepackage{commath}


\begin{document}


\centerline{\large \sc Topics in Geometry - Homework 6}
\centerline{\large  Sophie Stephenson \ November 13, 2019}
\bigskip

\begin{enumerate}


\item[1.)] Suppose that $P$ and $Q$ are points in $\bigH$ so that $Re(P) = Re(Q)$. Assume that $Im(P) > Im(Q)$. Find an expression for the distance between $P$ and $Q$ by finding a Mobius transformation that takes the hyperbolic line between them to the imaginary axis and using the fact that Mobius transformations preserve distance between points.
\begin{proof}[Solution] Since $Re(P) = Re(Q)$, the Mobius transformation $T(z) = z - Re(P)$ takes $P$ and $Q$, and any points on the hyperbolic line between them, to the imaginary axis. Thus, we have that $T(P) = P - Re(P) = iIm(P)$ and $T(Q) = iIm(Q)$. Since $T$ preserves hyperbolic distance, and since $T(P)$ and $T(Q)$ are both on the imaginary axis, we have
$$d_{\bigH}(P, Q) = d_{\bigH}(T(P), T(Q)) = \ln\Big(\tfrac{Im(P)}{Im(Q)}\Big).$$
\end{proof}
\bigskip

\item[2.)] Find the distance between each of the following pairs of points in $\bigH$ by finding a Mobius transformation that takes the hyperbolic line between each pair of points to the imaginary axis, and then measuring the distance between the images of these points on the imaginary axis, where the expression for distance between points is easy: \\
\begin{enumerate}
	\item[(a)] $P = i$ and $Q = 1 + 2i$
	\begin{proof}[Solution] Using our formula from problem 2 in the last homework, we find that the center of the semicircle that passes through $P$ and $Q$ is 
	$$c = \frac{4 - 1}{2(1 - 0)} + \frac{0 + 1}{2} = 2.$$
	The radius $R = |i - 2| = \sqrt{5}$, so the endpoints at infinity are $2 + \sqrt{5}$ and $2 - \sqrt{5}$. We can now define 
	$$T(z) = \frac{z - 2 - \sqrt{5}}{z - 2 + \sqrt{5}}$$
	where $T$ sends $2 + \sqrt{5}$ to 0, $2 - \sqrt{5}$ to $\infty$, and the rest of the semicircle to the imaginary axis. Since $T$ preserves hyperbolic distance, and since $T(P)$ and $T(Q)$ are on the imaginary axis with $T(P) > T(Q)$, we know that 
	$$d_{\bigH}(P, Q) = d_{\bigH}(T(P), T(Q)) = \ln\Bigg(\frac{Im(T(P))}{Im(T(Q)}\Bigg).$$
	In order to find $Im(T(P))$ and $Im(T(Q))$, we first need to compute $T(P)$ and $T(Q)$. For $T(P)$, we have:
	\begin{align*}
	T(P) &= \frac{i - 2 - \sqrt{5}}{i - 2 + \sqrt{5}}\\
		&= \frac{((-2 - \sqrt{5}) + i)((-2 + \sqrt{5}) - i)}{((-2 + \sqrt{5}) + i)((-2 + \sqrt{5}) - i )}\\
		&= \frac{\sqrt{5}i}{5 - 2\sqrt{5}}\\
		&= \frac{\sqrt{5}i(5 + 2\sqrt{5})}{(5 - 2\sqrt{5})(5 + 2\sqrt{5})}\\
		&= i(\sqrt{5} + 2),
	\end{align*}
	and for $T(Q)$:
	\begin{align*}
	T(Q) &= \frac{1 + 2i - 2 - \sqrt{5}}{1 + 2i - 2 + \sqrt{5}}\\
	&= \frac{((-1 - \sqrt{5}) + 2i)((-1 + \sqrt{5}) - 2i)}{((-1 +\sqrt{5}) + 2i)((-1 + \sqrt{5}) - 2i)}\\
	&= \frac{2\sqrt{5}i}{5 - \sqrt{5}}\\
	&= \frac{2\sqrt{5}i(5 + \sqrt{5})}{(5 - \sqrt{5})(5 + \sqrt{5})}\\
	&= \frac{i(\sqrt{5} + 1)}{2}.
	\end{align*}
	Using our values for $T(P)$ and $T(Q)$, we find that 
	$$d_{\bigH}(P, Q) = \ln\bigg( \frac{Im(T(P))}{Im(T(Q))} \bigg) = \ln \bigg( \frac{\sqrt{5} + 2}{\frac{\sqrt{5} + 1}{2}} \bigg) =\ln \bigg(  \frac{2\sqrt{5} + 4}{\sqrt{5} + 1}  \bigg).$$
	\end{proof}
	\item[(b)] $P = 1 + 2i$ and $Q = -1 + 2i$
	\begin{proof}[Solution] First, we find details about the semicircle passing through $P$ and $Q$. Clearly, the center of this circle is 0, and the radius is $|1 + 2i| = \sqrt{5}$. Thus, the endpoints at infinity of the semicircle are $-\sqrt{5}$ and $\sqrt{5}$. Using this information, we can define
	$$T(z) = \frac{z - \sqrt{5}}{z + \sqrt{5}}$$
	where $T$ sends $\sqrt{5}$ to 0, $-\sqrt{5}$ to $\infty$, and the rest of the semicircle to the imaginary axis. We solve for $T(P)$:
	\begin{align*}
	T(P) &= \frac{1 + 2i - \sqrt{5}}{1 + 2i + \sqrt{5}}\\
	&= \frac{((1 - \sqrt{5}) + 2i)((1 + \sqrt{5}) - 2i)}{((1 + \sqrt{5}) + 2i)((1 + \sqrt{5}) - 2i)}\\
	&= \frac{2\sqrt{5}i}{5 + \sqrt{5}}\\
	&= \frac{2\sqrt{5}i(5 - \sqrt{5})}{(5 + \sqrt{5})(5 - \sqrt{5})}\\
	&= \frac{i(\sqrt{5} -1)}{2}.
	\end{align*}
	Now, solve for $T(Q)$:
	\begin{align*}
	T(Q) &= \frac{-1 + 2i - \sqrt{5}}{-1 + 2i + \sqrt{5}}\\
	&= \frac{((-1 - \sqrt{5}) + 2i)((-1 + \sqrt{5}) - 2i)}{((-1 + \sqrt{5}) + 2i)((-1 + \sqrt{5}) - 2i)}\\
	&= \frac{2\sqrt{5}i}{5 - \sqrt{5}}\\
	&= \frac{2\sqrt{5}i(5 + \sqrt{5})}{(5 - \sqrt{5})(5 + \sqrt{5})}\\
	&= \frac{i(\sqrt{5} + 1)}{2}.
	\end{align*}
	 It follows that 
	 $$d_{\bigH}(P, Q) =d_{\bigH}(T(P), T(Q)) =  \ln \bigg( \frac{Im(T(P))}{Im(T(Q))} \bigg) = \ln \bigg( \frac{\frac{\sqrt{5} -1}{2}}{\frac{\sqrt{5} + 1}{2}} \bigg) = \ln \bigg(\frac{\sqrt{5} -1}{\sqrt{5} + 1} \bigg).$$
	\end{proof}
\end{enumerate}
\bigskip



\item[3.)] Let $ai$ and $bi$ be points on the imaginary axis and suppose $b > a$. Find an expression for the hyperbolic midpoint of the vertical line segment between $ai$ and $bi$. It won't be the same as the Euclidean midpoint.
\begin{proof}[Solution] We want to find $xi$ on the imaginary axis such that $xi$ is equidistant from $ai$ and $bi$. We can solve for $x$:
\begin{align*}
\ln\bigg(\frac{b}{x}\bigg) = \ln\bigg(\frac{x}{a}\bigg) &\implies \ln(b) - \ln(x) = \ln(x) - \ln(a)\\
&\implies \ln(b) + \ln(a) = 2\ln(x)\\
&\implies \tfrac{1}{2}\ln(ba) = \ln(x)\\
&\implies \ln\big(\sqrt{ba}\ \big) = \ln(x)\\
&\implies \sqrt{ba} = x
\end{align*}
Thus, the hyperbolic midpoint of the line segment between $ai$ and $bi$ is $\sqrt{ab}$. \\
\end{proof}
\bigskip

\item[4.)] Consider the hyperbolic line which is a semicircle orthogonal to the real axis given parametrically by $z(t) = R\cos(t) + iR\sin(t)$. Show that the hyperbolic length of the arc of the circle between two angles $\theta_1$ and $\theta_2$ with $0 < \theta_1 < \theta_2 < \pi$ is given by 
$$\ln \frac{\abs{\csc (\theta_2) - \cot (\theta_2)}}{\abs{\csc(\theta_1) - \cot(\theta_1)}}.$$
\begin{proof} We know that the hyperbolic arc length of the circle is given by 
$$\int_{\theta_1}^{\theta_2} \frac{|z'(t)|}{y(t)}dt = \int_{\theta_1}^{\theta_2} \frac{|-R\sin(t) + iR\cos(t)|}{R\sin(t)}dt,$$
and we can solve to find
\begin{align*}
	\int_{\theta_1}^{\theta_2} \frac{|-R\sin(t) + iR\cos(t)|}{R\sin(t)}dt &= \int_{\theta_1}^{\theta_2}\frac{\sqrt{(R\sin(t))^2 + (R\cos(t))^2}}{R\sin(t)}dt\\
	&= \int_{\theta_1}^{\theta_2}\frac{\sqrt{R^2(\sin^2(t) + \cos^2(t))}}{R\sin(t)}dt\\
	&= \int_{\theta_1}^{\theta_2}\frac{1}{\sin(t)}dt\\
	&= -\ln\abs{\csc(t) + \cot(t)} \ \bigg\rvert_{\theta_1}^{\theta_2}. \\
\end{align*}
Then, we use trig identities to get the form we want and evaluate. 
\begin{align*}
	-\ln\abs{\csc(t) + \cot(t)} \ \bigg\rvert_{\theta_1}^{\theta_2} &= \ln \frac{1}{\abs{\csc(t) + \cot(t)}} \ \bigg\rvert_{\theta_1}^{\theta_2} \\
	&= \ln \frac{1}{\abs{\frac{1 + \cos(t)}{\sin(t)}}}\ \bigg\rvert_{\theta_1}^{\theta_2} \\
	&= \ln \abs{\frac{\sin(t)}{1 + \cos(t)}} \ \bigg\rvert_{\theta_1}^{\theta_2} \\
	&= \ln \abs{\tan(\tfrac{t}{2})}\ \bigg\rvert_{\theta_1}^{\theta_2} \\
	&= \ln \abs{\csc(t) - \cot(t)} \ \bigg\rvert_{\theta_1}^{\theta_2} \\
	&= \ln \abs{\csc(\theta_2) - \cot(\theta_2)} - \ln \abs{\csc(\theta_1) - \cot(\theta_1)}\\
	&= \ln \frac{\abs{\csc(\theta_2) - \cot(\theta_2)}}{\abs{\csc(\theta_1) - \cot(\theta_1)}},
\end{align*}
as desired.\\
\end{proof}
\bigskip

\item[5.)] In this problem, you'll derive a second formula for measuring distance between points on the circle $z(t) = R\cos(t) + iR\sin(t)$. Let $\theta_k$ for $k = 1, 2$ be defined as before. Let $z_1 = x_1 + iy_1$ and $z_2 = x_2 + iy_2$ be the points on the circle corresponding to $\theta_1$ and $\theta_2$. Show that $\csc(\theta_k) = R/y_k$ and $\cot(\theta_k) = x_k / y_k$. Using the formula in the previous problem, show that the hyperbolic length of the arc of the circle between $z_1$ and $z_2$ is given by 
$$\ln \frac{\abs{y_1(x_2 - R)}}{\abs{y_2(x_1 - R)}}.$$

\begin{proof} First, we will show that $\csc(\theta_k) = R/y_k$ and $\cot(\theta_k) = x_k / y_k$ for $k = 1, 2$. Since $z(\theta_1) = x_1 + iy_1$ and $z(\theta_2) = x_2 + iy_2$, we have
$$x_1 = R\cos(\theta_1) \implies \frac{x_1}{R} = \cos(\theta_1), \ \ \ \ y_1 = R\sin(\theta_1) \implies \frac{y_1}{R} = \sin(\theta_1),$$
and similarly
$$x_2 = R\cos(\theta_2) \implies \frac{x_2}{R} = \cos(\theta_2), \ \ \ \ y_2 = R\sin(\theta_2) \implies \frac{y_2}{R} = \sin(\theta_2).$$
Thus, we have:
$$\csc(\theta_1) = \frac{1}{\sin(\theta_1)} = \frac{R}{y_1}, \ \ \ \ \csc(\theta_2) = \frac{1}{\sin(\theta_2)} = \frac{R}{y_2};$$
$$\cot(\theta_1) = \frac{\cos(\theta_1)}{\sin(\theta_1)} = \frac{\tfrac{x_1}{R}}{\tfrac{y_1}{R}} = \frac{x_1}{y_1}, \ \ \ \ \cot(\theta_2) = \frac{\cos(\theta_2)}{\sin(\theta_2)} = \frac{\tfrac{x_2}{R}}{\tfrac{y_2}{R}} = \frac{x_2}{y_2}.$$\\
Next, we will plug these values into the formula from problem 4. We see that
\begin{align*}
\ln \frac{\abs{\csc (\theta_2) - \cot (\theta_2)}}{\abs{\csc(\theta_1) - \cot(\theta_1)}} &=  \ln \frac{\abs{\frac{R}{y_2} - \frac{x_2}{y_2}}}{\abs{\frac{R}{y_1} - \frac{x_1}{y_1}}}\\
&=  \ln\abs{ \frac{\frac{R - x_2}{y_2}}{\frac{R - x_1}{y_1}}}\\
&= \ln \abs{\frac{y_1(R-x_2)}{y_2(R - x_1)}}\\
&= \ln \abs{\frac{y_1(x_2-R)}{y_2(x_1-R)}},\\
\end{align*}
which is what we wanted to show.\\ 
\end{proof}
\bigskip


\end{enumerate}


\end{document}